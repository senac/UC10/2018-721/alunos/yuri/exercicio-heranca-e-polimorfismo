package Testes;


import Exercicio3.Diagrama2.Animal;
import Exercicio3.Diagrama2.Cachorro;

import java.util.ArrayList;
import java.util.List;
import Exercicio3.Diagrama1.Comunicar;
import Exercicio3.Diagrama1.Miseravel;
import Exercicio3.Diagrama1.Pessoa;
import Exercicio3.Diagrama1.Pobre;
import Exercicio3.Diagrama1.Rica;
import Exercicio3.Diagrama2.Gato;

public class TestC {

    public static void main(String[] args) {

        Rica rica = new Rica();
        Pobre pobre = new Pobre();
        Miseravel miseravel = new Miseravel();
        Gato preto = new Gato();
        Gato chanel = new Gato();
        Gato branco = new Gato();
        

        List<Pessoa> pessoas = new ArrayList<>();

        List<Comunicar> lista = new ArrayList<>();

        pessoas.add(pobre);
        pessoas.add(rica);
        pessoas.add(miseravel);

        lista.add(branco);
        lista.add(chanel);
        lista.add(preto);
        lista.add(rica);
        lista.add(pobre);
        lista.add(miseravel);

        for (Comunicar c : lista) {

            if (c instanceof Animal) {
                Animal a = (Animal) c;
                a.caminha();
            }

        }

    }

}
