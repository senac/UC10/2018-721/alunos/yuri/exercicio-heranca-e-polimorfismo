package Exercicio3.Diagrama2;

import Exercicio3.Diagrama1.Comunicar;

public abstract class Animal  implements Comunicar{

    private String nome;
    private String raca;

    public Animal() {
    }

    public Animal(String nome) {
        this.nome = nome;
    }

    public void caminha() {
        System.out.println("Animal caminhando ...");
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

   

}
