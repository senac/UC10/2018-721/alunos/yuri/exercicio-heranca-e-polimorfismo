/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;

import java.util.Scanner;

/**
 *
 * @author sala304b
 */
public class ContaTeste {
    public static void main(String[] args) {
 
        ContaCorrente contaC = new ContaCorrente();
        ContaPoupanca contaP = new ContaPoupanca();
        
        contaC.depositar(100);
        System.out.println("Conta Corrente - " + contaC.tostring());    
        
        contaC.sacarC(3);
            System.out.println("Saldo Conta Corrente depois de sacar - " + contaC.tostring());
        System.out.println("----------------------------------------------");
        
        contaP.depositar(200);
        System.out.println("Conta Poupança - " + contaP.tostring() );
        contaP.sacarP(30);
    
            System.out.println("Conta Poupança depois de sacar - " + contaP.tostring());
    
}   
}
