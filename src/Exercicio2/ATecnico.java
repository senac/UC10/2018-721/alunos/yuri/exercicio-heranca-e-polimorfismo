/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercicio2;

/**
 *
 * @author sala304b
 */
public class ATecnico extends Assistente{
        private double bonus;

    public ATecnico(String matricula) {
        super(matricula);
    }

    public ATecnico(String matricula, double bns) {
        super(matricula);
        this.bonus = bns;
    }

    @Override
    public double getSalario() {

        return super.getSalario() + this.getBonus();

    }

    public double getBonus() {
        return bonus;
    }

}