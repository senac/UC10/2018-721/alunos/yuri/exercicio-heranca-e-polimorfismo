/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercicio2;

/**
 *
 * @author sala304b
 */
public class AAdministrativo extends Assistente{
    private String turno ; 
    private final double adicionalNoturno = 0.35; 
    
    
        public AAdministrativo(String matricula , String turno) {
        super(matricula);
        this.turno = turno;
    }

    @Override
    public double getSalario() {
    
        return  this.turno.equalsIgnoreCase("Nortuno") ? super.getSalario() + (super.getSalario() * adicionalNoturno)  : super.getSalario() ;
        

    }
}