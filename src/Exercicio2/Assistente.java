/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercicio2;

import java.util.Objects;

/**
 *
 * @author sala304b
 */
public class Assistente extends Funcionario{
    private String matricula;

        public Assistente(String matricula) {
        this.matricula = matricula;
    }

    public String getMatricula() {
        return matricula;
    }

    @Override
    public int hashCode() {
        int hash = 8;
        hash = 81 * hash + Objects.hashCode(this.matricula);
        return hash;
    }

    @Override
    public boolean equals(Object objeto) {
        if (this == objeto) {
            return true;
        }
        if (objeto == null) {
            return false;
        }
        if (getClass() != objeto.getClass()) {
            return false;
        }
        final Assistente other = (Assistente) objeto;
        if (!Objects.equals(this.matricula, other.matricula)) {
            return false;
        }
        return true;
    }

    
    

    @Override
    public String toString() {
        return "Nome:" + this.getNome() + " - Matricula:" + this.matricula ; 
    }

}
    
   
