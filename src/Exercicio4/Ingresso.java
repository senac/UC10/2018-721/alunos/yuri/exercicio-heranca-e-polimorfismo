package Exercicio4;

public abstract class Ingresso {

    protected double valor;

    public Ingresso(double valor) {
        this.valor = valor;
    }

    public void imprimeValor() {
        System.out.println(StringUtils.formatMonetario(valor));
    }

}
