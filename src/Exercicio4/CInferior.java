
package Exercicio4;


public class CInferior extends Vip{
    
    private String localizacao;
    
    public CInferior(double valor , String localizacao) {
        super(valor);
        this.localizacao = localizacao;
    }
    
    public void ImprimirLocalizacao(){
        System.out.println("Localização:" + this.localizacao);
    }
    
    
}
