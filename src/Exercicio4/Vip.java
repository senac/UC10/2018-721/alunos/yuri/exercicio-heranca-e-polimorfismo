package Exercicio4;

public  class Vip extends Ingresso {

    private final double adicional = 70;

    public Vip(double valor) {
        super(valor);
    }

    @Override
    public final void imprimeValor() {
        System.out.println(StringUtils.formatMonetario(this.valor + this.adicional));
    }

}
