package Exercicio4;

public class CSuperior extends Vip {

    private double adicional = 100;

    public CSuperior(double valor) {
        super(valor);
    }

    public double getValor() {
        return valor + this.adicional;
    }

}
