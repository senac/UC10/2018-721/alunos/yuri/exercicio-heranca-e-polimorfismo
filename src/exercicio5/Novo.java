package exercicio5;

import Exercicio4.StringUtils;

public class Novo extends Imovel {

    private final double adicional;

    public Novo(String endereco, double preco, double adicional) {
        super(endereco, preco);
        this.adicional = adicional;
    }

    public void getValorImovel() {
        System.out.println(StringUtils.formatMonetario(this.getPreco()));
    }

    public double getAdicional() {
        return adicional;
    }

    @Override
    public double getPreco() {
        return this.preco + this.adicional;
    }

}
